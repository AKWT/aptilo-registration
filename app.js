


angular.module('formApp', ['ngAnimate', 'ui.router'])

// configuring routes 

.config(function($stateProvider, $urlRouterProvider) {

        // route to show basic form (/form)
        $stateProvider.state('form', {
            url: '/form',
            templateUrl: 'form.html',
            controller: 'formController'
        });
        

        // each of these nested urls sections have their own html template

        // step1
        $stateProvider.state('form.step1', {
            url: '/step1',
            templateUrl: 'form-step1.html'
        });

        // step2
        $stateProvider.state('form.step2', {
            url: '/step2',
            templateUrl: 'form-step2.html'
        });

        // step3
        $stateProvider.state('form.step3', {
            url: '/step3',
            templateUrl: 'form-step3.html'
        });
       
    // it catches all route and redirects to the form page
        // if no state is given in url it will redirect to step1 state
    $urlRouterProvider.otherwise('/form/step1');
})

// our controller for the form
.controller('formController', function($scope) {
    
    // all form data is stored in this object
    // on page refresh all formData will be reset.

    $scope.formData = {};


        //================== SPECIAL NOTE FOR APTILO =======================
        //
        // If we do not want to reset formData object on page refresh, we can make separate controller
        // for each templateUrl and can define each separate object in these controllers, and will reset
        // in the first line. Then on page refresh, only that page's form fields will be reset.
        //
        //==================================================================
    
    // function to submit the form, called on submit button, in index.html page.
    $scope.processForm = function() {
        console.log($scope.formData);
        alert('form submission service of angularJS should run at server side using API.');

    };
    
});

