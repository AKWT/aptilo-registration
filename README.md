# README #

I have selected Task-3 “Registration flow prototype”. Following instructions should be followed to run this project.

Following are just for your information:
-	This project is using AngularJS 1.4.0.
-	This is single page application, with HTML / Javascript. 
-	No server side code is provided in the solution, however, some handles have been placed in controller to use server side APIs, if required at any later stage.
-	I have downloaded this project from internet to save time, just made required changes 
-	I have used this downloaded project just due to following:
	o	To save time
	o	It’s using bootstrap 3.x.x which is mobile first.
	o	Its animations were perfect fit for the required solution.
-	I have implemented all validations as extra task, not mentioned into the requirement document, but generally these must be required in any form development, however, it is recommended to use validations at server side too.
